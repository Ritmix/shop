import requests
from django.shortcuts import render
from rest_framework import serializers, viewsets, pagination

from core.models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('name', 'price', 'photo')


class ProductPaginator(pagination.PageNumberPagination):
    page_size = 8


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    pagination_class = ProductPaginator


def index(request):
    context = {}
    if request.is_ajax():
        page = request.GET.get('page', 1)
        products = requests.get('api/products/?format=json&page=%i' % page)
        context['products'] = products
        return render(request, 'showcase.html', context)
    context['products'] = Product.objects.all()
    return render(request, 'base.html', context)
