from django.db import models


class Product(models.Model):
    name = models.CharField(u'Название', max_length=255)
    category = models.CharField(u'Категория', max_length=255)
    price = models.IntegerField(u'Цена')
    photo = models.ImageField(u'Фото', blank=True, null=True)

    def __str__(self):
        return '%s - %i' % (self.name, self.price)
