from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers

from core.views import ProductViewSet, index


router = routers.DefaultRouter()
router.register(r'products', ProductViewSet, 'products')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls, namespace='api')),
    url(r'^products/', index),
]


